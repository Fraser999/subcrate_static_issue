Minimal example showing unexpected duplicated static value.

```
git clone https://gitlab.com/Fraser999/subcrate_static_issue.git
cd subcrate_static_issue
cargo test -- --nocapture
```
