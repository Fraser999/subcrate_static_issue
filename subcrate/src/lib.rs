extern crate subcrate_static_issue;

pub fn subcrate_call_increment() {
    subcrate_static_issue::increment(&env!("CARGO_PKG_NAME"));
}
