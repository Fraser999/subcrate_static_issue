#[cfg(test)]
extern crate subcrate;

static mut VALUE: usize = 0;

pub fn increment(caller: &str) {
    unsafe {
        VALUE += 1;
        println!(
            "VALUE is {} after being incremented by {}.",
            VALUE, caller
        );
    }
}

#[cfg(test)]
#[test]
fn issue() {
    let caller = env!("CARGO_PKG_NAME");
    increment(&caller);
    increment(&caller);
    subcrate::subcrate_call_increment();
    increment(&caller);
    subcrate::subcrate_call_increment();
}
